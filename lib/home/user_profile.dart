import 'package:arkamaya_project/home/size_config.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_skeleton/loader_skeleton.dart';

import 'controller/user_controller.dart';
import 'model/user_model.dart';

class ProfilePages extends StatefulWidget {
  ProfilePages({ Key? key, required this.id}) : super(key: key);

  final int id;

  @override
  _ProfilePagesState createState() => _ProfilePagesState();
}
class _ProfilePagesState extends State<ProfilePages> {
  UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return Scaffold(
              backgroundColor: Color(0xffF8F8FA),
              body:FutureBuilder<UserDetailResponse?>(
                  future: userController.getUserDetail(id: widget.id),
                  builder: (context, snapshot) {

                  if(snapshot.hasData){
                    var userData = snapshot.data;
                    if (userData?.data != null) {
                      return Stack(
                        children: <Widget>[
                          Container(
                            color: Colors.blue[600],
                            height: 40 * SizeConfig.heightMultiplier,
                            child: Padding(
                              padding:  EdgeInsets.only(left: 30.0, right: 30.0, top: 10 * SizeConfig.heightMultiplier),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      ClipRRect(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(50.0),
                                            bottomLeft: Radius.circular(50.0),
                                            bottomRight: Radius.circular(50.0),
                                            topRight: Radius.circular(50.0)),
                                        child: CachedNetworkImage(
                                          fit: BoxFit.cover,
                                          height: 90,
                                          width: 90,
                                          imageUrl:userData?.data?.avatar ?? "https://i.pravatar.cc/350",
                                          placeholder: (context, url) => Container(
                                              width: 30, height: 30, child: CircularProgressIndicator()),
                                          errorWidget: (context, url, error) =>
                                              Center(child: new Icon(Icons.error)),
                                        ),
                                      ),
                                      SizedBox(width: 5 * SizeConfig.widthMultiplier,),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text("${userData?.data?.first_name} ${userData?.data?.last_name}", style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 3 * SizeConfig.textMultiplier,
                                              fontWeight: FontWeight.bold
                                          ),),
                                          SizedBox(height: 1 * SizeConfig.heightMultiplier,),
                                          Row(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Image.asset(
                                                    "assets/fb.png",
                                                    height: 3 * SizeConfig.heightMultiplier,
                                                    width: 3 * SizeConfig.widthMultiplier,
                                                  ),
                                                  SizedBox(width: 2 * SizeConfig.widthMultiplier,),
                                                  Text("Arkamaya", style: TextStyle(
                                                    color: Colors.white60,
                                                    fontSize: 1.5 * SizeConfig.textMultiplier,
                                                  ),),
                                                ],
                                              ),
                                              SizedBox(width: 7 * SizeConfig.widthMultiplier,),
                                              Row(
                                                children: <Widget>[
                                                  Image.asset(
                                                    "assets/insta.png",
                                                    height: 3 * SizeConfig.heightMultiplier,
                                                    width: 3 * SizeConfig.widthMultiplier,
                                                  ),
                                                  SizedBox(width: 2 * SizeConfig.widthMultiplier,),
                                                  Text("Arkamaya", style: TextStyle(
                                                    color: Colors.white60,
                                                    fontSize: 1.5 * SizeConfig.textMultiplier,
                                                  ),),
                                                ],
                                              )
                                            ],
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 3 * SizeConfig.heightMultiplier,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        children: <Widget>[
                                          Text("10.2K", style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 3 * SizeConfig.textMultiplier,
                                              fontWeight: FontWeight.bold
                                          ),),
                                          Text("Followers", style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 1.9 * SizeConfig.textMultiplier,
                                          ),),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Text("543", style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 3 * SizeConfig.textMultiplier,
                                              fontWeight: FontWeight.bold
                                          ),),
                                          Text("Following", style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 1.9 * SizeConfig.textMultiplier,
                                          ),),
                                        ],
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Colors.white60),
                                          borderRadius: BorderRadius.circular(5.0),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text("EDIT PROFILE", style: TextStyle(
                                              color: Colors.white60,
                                              fontSize: 1.8 * SizeConfig.textMultiplier
                                          ),),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding:  EdgeInsets.only(top: 35 * SizeConfig.heightMultiplier),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(30.0),
                                    topLeft: Radius.circular(30.0),
                                  )
                              ),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding:  EdgeInsets.only(left: 30.0, top: 3 * SizeConfig.heightMultiplier),
                                      child: Text("My Albums", style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 2.2 * SizeConfig.textMultiplier
                                      ),),
                                    ),
                                    SizedBox(height: 3 * SizeConfig.heightMultiplier,),
                                    Container(
                                      height: 37 * SizeConfig.heightMultiplier,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: <Widget>[
                                          _myAlbumCard("assets/travelfive.png", "assets/traveltwo.png", "assets/travelsix.png", "assets/travelthree.png", "+178", "Best Trip"),
                                          _myAlbumCard("assets/travelsix.png", "assets/travelthree.png", "assets/travelfour.png", "assets/travelfive.png", "+18", "Hill Lake Tourism"),
                                          _myAlbumCard("assets/travelfive.png", "assets/travelsix.png", "assets/traveltwo.png", "assets/travelone.png", "+1288", "The Grand Canyon"),
                                          SizedBox(width: 10 * SizeConfig.widthMultiplier,),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 3 * SizeConfig.heightMultiplier,),
                                    Padding(
                                      padding:  EdgeInsets.only(left: 30.0, right: 30.0),
                                      child: Row(
                                        children: <Widget>[
                                          Text("Favourite places", style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 2.2 * SizeConfig.textMultiplier
                                          ),),
                                          Spacer(),
                                          Text("View All", style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 1.7 * SizeConfig.textMultiplier
                                          ),),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 3 * SizeConfig.heightMultiplier,),
                                    Container(
                                      height: 20 * SizeConfig.heightMultiplier,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: <Widget>[
                                          _favoriteCard("assets/travelfive.png"),
                                          _favoriteCard("assets/travelthree.png"),
                                          _favoriteCard("assets/travelfive.png"),
                                          SizedBox(width: 10 * SizeConfig.widthMultiplier,)
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 3 * SizeConfig.heightMultiplier,)
                                  ],
                                ),
                              ),
                            ),
                          ),

                          Positioned(
                            left: 0,
                            top: 30,
                            child: InkWell(
                              onTap: (){
                                Get.back();
                              },
                              child: Container(
                                height: 50,
                                width: 50,
                                child: Icon(
                                  Icons.close,color: Colors.white,
                                ),
                              ),
                            ),
                          ),

                        ],
                      );
                    }
                    else {
                      return const Center(child: Text("No Data"));
                    }
                  }

                  else if(snapshot.hasError){
                    return Container(
                      child: const Center(
                        child: Text("ERROR"),
                      ),
                    );
                  }

                  return Container(
                    height: MediaQuery.of(context).size.height,
                    child: CardProfileSkeleton(
                      isCircularImage: true,
                      isBottomLinesActive: true,
                    ),
                  );
                }
              ),

            );
          },
        );
      },
    );
  }

  _myAlbumCard(String asset1, String asset2, String asset3, String asset4, String more, String name) {
    return Padding(
      padding: const EdgeInsets.only(left: 40.0),
      child: Container(
        height: 37 * SizeConfig.heightMultiplier,
        width: 60 * SizeConfig.widthMultiplier,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            border: Border.all(color: Colors.grey, width: 0.2)
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.asset(asset1, height: 27 * SizeConfig.imageSizeMultiplier, width: 27 * SizeConfig.imageSizeMultiplier, fit: BoxFit.cover,),
                  ),
                  Spacer(),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.asset(asset2, height: 27 * SizeConfig.imageSizeMultiplier, width: 27 * SizeConfig.imageSizeMultiplier, fit: BoxFit.cover,),
                  ),
                ],
              ),
              SizedBox(height: 1 * SizeConfig.heightMultiplier,),
              Row(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.asset(asset3, height: 27 * SizeConfig.imageSizeMultiplier, width: 27 * SizeConfig.imageSizeMultiplier, fit: BoxFit.cover,),
                  ),
                  Spacer(),
                  Stack(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: Image.asset(asset4, height: 27 * SizeConfig.imageSizeMultiplier, width: 27 * SizeConfig.imageSizeMultiplier, fit: BoxFit.cover,),
                      ),
                      Positioned(
                        child: Container(
                          height: 27 * SizeConfig.imageSizeMultiplier,
                          width: 27 * SizeConfig.imageSizeMultiplier,
                          decoration: BoxDecoration(
                              color: Colors.black38,
                              borderRadius: BorderRadius.circular(10.0)
                          ),
                          child: Center(
                            child: Text(more, style: TextStyle(
                              color: Colors.white,
                              fontSize: 2.5 * SizeConfig.textMultiplier,
                            ),),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Padding(
                padding:  EdgeInsets.only(left: 10.0, top: 2 * SizeConfig.heightMultiplier),
                child: Text(name, style: TextStyle(
                    color: Colors.black,
                    fontSize: 2 * SizeConfig.textMultiplier,
                    fontWeight: FontWeight.bold
                ),),
              )
            ],
          ),
        ),
      ),
    );
  }

  _favoriteCard(String s) {
    return Padding(
      padding: const EdgeInsets.only(left: 40.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Image.asset(s, height: 20 * SizeConfig.heightMultiplier, width: 70 * SizeConfig.widthMultiplier, fit: BoxFit.cover,),
      ),
    );
  }
}