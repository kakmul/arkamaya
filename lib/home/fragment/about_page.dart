import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../my_colors.dart';

class AboutFragment extends StatefulWidget {
  @override
  _AboutFragmentState createState() => _AboutFragmentState();
}

class _AboutFragmentState extends State<AboutFragment> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

    final imgview = Padding(
        padding: const EdgeInsets.all(10.0),
        child:Center(
          child: Container(
            padding: EdgeInsets.only(left:10,right: 10),
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 25,
                        width:70,
                        child:Image(image: AssetImage("assets/logo.png"),height: 25,width: 30,),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Arkamaya.",
                                textAlign:  TextAlign.center,
                                style: TextStyle(fontFamily: "Regular", fontSize: 16.0, color: Colors.black,),),
                              Text("Oleh PT. Arkamaya",
                                textAlign:  TextAlign.center,
                                style: TextStyle(fontFamily: "Regular", fontSize: 12.0, color: MyColors.primary,),),
                            ],
                          )
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 25,
                        width:70,
                        child:Image(image: AssetImage("assets/ic_apple_store_outline.png"),height: 25,width: 30,),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Download Apple Store",
                                textAlign:  TextAlign.center,
                                style: TextStyle(fontFamily: "Regular", fontSize: 16.0, color: Colors.black,),),
                              Text("Versi 1.0.1.5",
                                textAlign:  TextAlign.center,
                                style: TextStyle(fontFamily: "Regular", fontSize: 12.0, color: MyColors.primary,),),
                            ],
                          )
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Row(
                        children: <Widget>[
                          Container(
                            height: 25,
                            width:70,
                            child:Image(image: AssetImage("assets/ic_google_play.png"),height: 25,width: 30,),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("Download Google Play",
                                    textAlign:  TextAlign.center,
                                    style: TextStyle(fontFamily: "Regular", fontSize: 16.0, color: Colors.black,),),
                                  Text("Versi 1.0.1.5",
                                    textAlign:  TextAlign.center,
                                    style: TextStyle(fontFamily: "Regular", fontSize: 12.0, color: MyColors.primary,),),
                                ],
                              )
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),

                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 25,
                        width:70,
                        child:Image(image: AssetImage("assets/correct.png"),height: 25,width: 30,),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Cak Cipta \u00a9 2023 PT. Arkamaya All Right Reserved",
                                textAlign:  TextAlign.left,
                                style: TextStyle(fontFamily: "Regular", fontSize: 12.0, color: MyColors.primary,),),
                            ],
                          )
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        )
    );


    final bantuan = Padding(
        padding: const EdgeInsets.all(10.0),
        child:Center(
          child: Container(
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 15,
                ),
                Text("Bantuan",
                  textAlign:  TextAlign.left,
                  style: TextStyle(fontFamily: "Regular", fontSize: 18.0, color: Colors.black,fontWeight: FontWeight.bold),),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 25,
                        width:70,
                        child:Image(image: AssetImage("assets/ic_instagram.png"),height: 25,width: 30,),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: InkWell(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("@arkamaya",
                                  textAlign:  TextAlign.center,
                                  style: TextStyle(fontFamily: "Regular", fontSize: 16.0, color: Colors.black,),),
                                Text("arkamaya",
                                  textAlign:  TextAlign.center,
                                  style: TextStyle(fontFamily: "Regular", fontSize: 12.0, color: MyColors.primary,),),
                              ],
                            ),
                            onTap: () async {

                            },
                          )
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 25,
                        width:70,
                        child:Image(image: AssetImage("assets/ic_whatsapp.png"),height: 25,width: 30,),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: InkWell(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("+62 895-2496-0107",
                                  textAlign:  TextAlign.center,
                                  style: TextStyle(fontFamily: "Regular", fontSize: 16.0, color: Colors.black,),),
                                Text("Hubungi Customer Service Kami",
                                  textAlign:  TextAlign.center,
                                  style: TextStyle(fontFamily: "Regular", fontSize: 12.0, color: MyColors.primary,),),
                              ],
                            ),
                            onTap: () async {
                              Get.snackbar(
                                "Gagal",
                                "Anda Belum Menginstall Whatsapp",
                                colorText: Colors.black,
                                backgroundColor: MyColors.primaryBackgroundRed,
                                icon: const Icon(Icons.check_circle,color: Colors.black,),
                              );
                            },
                          )
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 25,
                        width:70,
                        child:Image(image: AssetImage("assets/ic_whatsapp.png"),height: 25,width: 30,),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: InkWell(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("+62 0877-38000-770",
                                  textAlign:  TextAlign.center,
                                  style: TextStyle(fontFamily: "Regular", fontSize: 16.0, color: Colors.black,),),
                                Text("Hubungi Billing Support Kami",
                                  textAlign:  TextAlign.center,
                                  style: TextStyle(fontFamily: "Regular", fontSize: 12.0, color: MyColors.primary,),),
                              ],
                            ),
                            onTap: () async {
                              Get.snackbar(
                                "Gagal",
                                "Anda Belum Menginstall Whatsapp",
                                colorText: Colors.black,
                                backgroundColor: MyColors.primaryBackgroundRed,
                                icon: const Icon(Icons.check_circle,color: Colors.black,),
                              );
                            },
                          )
                      )

                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        )
    );



    return Container(
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: MyColors.backgroundColor,
        // appBar: AppBar(
        //   // actions: <Widget>[
        //   //   Padding(
        //   //     padding: EdgeInsets.only(right: 20),
        //   //     child: Image(
        //   //       image: AssetImage("images/qibus/qibus_gif_bell.gif"),
        //   //       height: 25,
        //   //       width: 25,
        //   //       color: Colors.white,
        //   //     ),
        //   //   )
        //   // ],
        //   title:  Text("About Apps"),
        //   leading: Container(),
        //   backgroundColor: MyColors.primary,
        //   elevation: 5.0,
        // ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
              children : <Widget>[
                imgview,
                bantuan
              ]),
        ),
      ),
    );
  }
}
