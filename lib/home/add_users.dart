import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import 'controller/user_controller.dart';

class AddUserPages extends StatefulWidget {
  const AddUserPages({Key? key}) : super(key: key);

  @override
  State<AddUserPages> createState() => _AddUserPagesState();
}

class _AddUserPagesState extends State<AddUserPages> {
  UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text( "Add Users"),
      ),
      body: Container(
        margin: EdgeInsets.only(top:30,right:20,left:20),
        child: FormBuilder(
          key: _formKey,
          // enabled: false,
          onChanged: () {
            _formKey.currentState!.save();
            debugPrint(_formKey.currentState!.value.toString());
          },
          autovalidateMode: AutovalidateMode.disabled,
          initialValue: const {
            'movie_rating': 5,
            'best_language': 'Dart',
            'age': '13',
            'gender': 'Male',
            'languages_filter': ['Dart']
          },
          skipDisabled: true,
          child: Column(
            children: <Widget>[
              FormBuilderTextField(
                name: 'name',
                decoration: InputDecoration(
                  labelText: "Masukan Nama Kamu",
                    labelStyle: TextStyle(
                        fontSize: 14
                    )
                ),
                onChanged: (val) {
                  print(val); // Print the text value write into TextField
                },
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              SizedBox(
                height: 20,
              ),
              FormBuilderTextField(
                name: 'job',
                decoration: InputDecoration(
                  labelText: "Masukan Pekerjaan Kamu",
                  labelStyle: TextStyle(
                    fontSize: 14
                  )
                ),
                onChanged: (val) {
                  print(val); // Print the text value write into TextField
                },
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Obx(() {
                        return ElevatedButton(
                          onPressed: () async {
                            if (_formKey.currentState?.saveAndValidate() ?? false) {
                              debugPrint(_formKey.currentState?.value.toString());
                              await userController.addUser(
                                params: jsonEncode(_formKey.currentState?.value),
                              );
                              _formKey.currentState?.reset();
                              FocusScope.of(context).unfocus();
                            } else {
                              debugPrint(_formKey.currentState?.value.toString());
                              debugPrint('validation failed');
                            }
                          },
                          child: userController.isLoading.value
                          ? Center(
                              child: Container(
                                width: 20,
                                height: 20,
                                child: CircularProgressIndicator(
                                  color: Colors.white,
                                ),
                              )
                          )
                          : const Text(
                            'Submit',
                            style: TextStyle(color: Colors.white),
                          ),
                        );
                      }
                    ),
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: OutlinedButton(
                      onPressed: () {
                        _formKey.currentState?.reset();
                      },
                      // color: Theme.of(context).colorScheme.secondary,
                      child: Text(
                        'Reset',
                        style: TextStyle(
                            color: Theme.of(context).colorScheme.secondary),
                      ),
                    ),
                  ),
                ],
              ),
            ],

          ),
        ),
      ),
    );
  }
}
