import 'package:arkamaya_project/home/add_users.dart';
import 'package:arkamaya_project/home/controller/user_controller.dart';
import 'package:arkamaya_project/home/model/user_model.dart';
import 'package:arkamaya_project/home/user_profile.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_skeleton/loader_skeleton.dart';

import 'fragment/about_page.dart';
import 'my_colors.dart';

class UserPages extends StatefulWidget {
  const UserPages({Key? key}) : super(key: key);

  @override
  State<UserPages> createState() => _UserPagesState();
}

class _UserPagesState extends State<UserPages> {
  UserController userController = Get.put(UserController());
  int selectedPage = 0;
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          //backgroundColor: MyColors.bgColors,
          appBar: AppBar(
            backgroundColor: MyColors.primary,
            title: Obx(() {
                return Text(userController.pageSelected.value == 0 ? "Home" : "About");
              }
            ),
          ),
          body:  TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: [
              FutureBuilder<UserResponse?>(
                future: userController.getUser(),
                builder: (context, snapshot) {

                  if (snapshot.hasData) {
                    var userData = snapshot.data;
                    if (userData?.data != null) {
                      return ListView.builder(
                          padding: EdgeInsets.only(bottom: 150),
                          itemCount: userData?.data?.length,
                          itemBuilder: (context, index) {
                            var user = userData!.data![index];
                            return InkWell(
                              onTap: (){
                                Get.to(() => ProfilePages(id:user.id ?? 0));
                              },
                              child: Container(
                                margin: EdgeInsets.only(left:20,right:20,top:10,bottom:10),
                                padding:  EdgeInsets.only(left:10,right:10,top:10,bottom:10),
                                height: 80,
                                decoration:  BoxDecoration(
                                  border: Border.all(
                                    width: 1, //
                                    color:  index.isEven ? MyColors.primaryBackground : Colors.white,
                                  ),
                                  color:Colors.white,
                                  borderRadius: const BorderRadius.only(
                                      topRight: Radius.circular(10.0),
                                      bottomRight: Radius.circular(10.0),
                                      topLeft: Radius.circular(10.0),
                                      bottomLeft: Radius.circular(10.0)),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: MyColors.grey_10,
                                        offset: Offset(0, 0),
                                        blurRadius: 1,
                                        spreadRadius: 1)
                                  ],
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(50.0),
                                              bottomLeft: Radius.circular(50.0),
                                              bottomRight: Radius.circular(50.0),
                                              topRight: Radius.circular(50.0)),
                                          child: CachedNetworkImage(
                                            fit: BoxFit.cover,
                                            height: 50,
                                            width: 50,
                                            imageUrl:user.avatar ?? "https://i.pravatar.cc/350",
                                            placeholder: (context, url) => Container(
                                                width: 30, height: 30, child: CircularProgressIndicator()),
                                            errorWidget: (context, url, error) =>
                                                Center(child: new Icon(Icons.error)),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              child: Text(
                                                "Nama ${user.first_name} ${user.last_name} ",
                                                style: const TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold
                                                ),
                                              ),
                                              margin: EdgeInsets.only(left: 10),
                                            ),
                                            Container(
                                              padding:  EdgeInsets.only(left:10,right:10,top:5,bottom:5),
                                              decoration:  const BoxDecoration(
                                                // border: Border.all(
                                                //   width: 1, //
                                                //   color:  index.isEven ? MyColors.primaryBackground : Colors.white,
                                                // ),

                                                color:MyColors.primaryLight,
                                                borderRadius: BorderRadius.only(
                                                    topRight: Radius.circular(10.0),
                                                    bottomRight: Radius.circular(10.0),
                                                    topLeft: Radius.circular(10.0),
                                                    bottomLeft: Radius.circular(10.0)),
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: MyColors.grey_10,
                                                      offset: Offset(0, 0),
                                                      blurRadius: 1,
                                                      spreadRadius: 1)
                                                ],
                                              ),
                                              child: Text(
                                                "Email :  ${user.email} ",
                                                style: const TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.black,
                                                ),
                                              ),
                                              margin: EdgeInsets.only(top:10,left: 10),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color:MyColors.primaryBackground,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20.0),
                                            bottomLeft: Radius.circular(20.0),
                                            bottomRight: Radius.circular(20.0),
                                            topRight: Radius.circular(20.0)),
                                      ),
                                      width:30,
                                      height:30,
                                      child: Center(
                                        child: Icon(
                                            Icons.arrow_forward_ios,
                                          color: Colors.white,
                                          size: 20,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    } else {
                      return const Center(child: Text("No Data"));
                    }
                  }

                  else if(snapshot.hasError){
                    return Container(
                      child: const Center(
                        child: Text("ERROR"),
                      ),
                    );
                  }


                  return Container(
                    height: MediaQuery.of(context).size.height,
                    child: CardListSkeleton(
                      isCircularImage: true,
                      isBottomLinesActive: true,
                      length: 10,
                    ),
                  );
                },
              ),
              AboutFragment()
            ],
          ),
            bottomNavigationBar: BottomAppBar(
              shape: CircularNotchedRectangle(),
              // padding: EdgeInsets.only(top: 10),
              // height: 60,
              // alignment: Alignment.topCenter,
              // decoration: const BoxDecoration(
              //   // border: Border.all(
              //   //   color: Colors.white,
              //   // ),
              //   // borderRadius: BorderRadius.only(
              //   //     topRight: Radius.circular(30.0),
              //   //     bottomRight: Radius.circular(0.0),
              //   //     topLeft: Radius.circular(30.0),
              //   //     bottomLeft: Radius.circular(0.0)),
              //   boxShadow: [
              //     BoxShadow(
              //         color: Colors.white,
              //         offset: Offset(0, 0),
              //         blurRadius: 0,
              //         spreadRadius: 0)
              //   ],
              // ),
              child: TabBar(
                labelColor: Colors.red,
                unselectedLabelColor: Colors.grey,
                labelPadding: EdgeInsets.symmetric(horizontal: 2),
                labelStyle:
                const TextStyle(fontWeight: FontWeight.bold),
                indicatorWeight: 1,
                indicatorColor: Colors.white,
                indicatorPadding: const EdgeInsets.all(5),
                isScrollable: false,
                physics: BouncingScrollPhysics(),
                onTap: (int index) {
                  userController.pageSelected.value = index;
                },
                enableFeedback: true,
                // Uncomment the line below and remove DefaultTabController if you want to use a custom TabController
                // controller: _tabController,
                tabs: [
                  Tab(
                      child: Column(
                        children: [
                          const Center(
                            child: Icon(Icons.home),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 5),
                              child: const Text(
                                "Home",
                                maxLines: 1,
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              )),
                        ],
                      )),


                  Tab(
                      child: Column(
                        children: [
                           Center(
                            child: Image.asset("assets/logo.png",
                              height: 20,
                              width:20,
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 5),
                              child: const Text(
                                "About",
                                maxLines: 1,
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              )),
                        ],
                      )),
                ],
              ),
            ),
         floatingActionButtonLocation:  FloatingActionButtonLocation.centerDocked,
          floatingActionButton:  InkWell(
            onTap: (){
              Get.to(() => AddUserPages());
            },
            child: Container(
              height: 60,
              width: 60,
              decoration: const BoxDecoration(
                color: MyColors.primary,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60.0),
                    bottomLeft: Radius.circular(60.0),
                    bottomRight: Radius.circular(60.0),
                    topRight: Radius.circular(60.0)),
                boxShadow: [
                  BoxShadow(
                    color: MyColors.primaryLight,
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Icon(Icons.add,color: Colors.white,),
            ),
          ),
        ),
      ),
    );
  }
}

