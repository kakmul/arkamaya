
class UserResponse {
  int? total;
  int? page;
  List<UserModel>? data;

  UserResponse({this.total, this.page, this.data});

  UserResponse.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    page = json['page'];
    if (json['data'] != null) {
      data = <UserModel>[];
      json['data'].forEach((v) {
        data!.add(new UserModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['page'] = this.page;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserDetailResponse {
  SupportModel? support;
  UserModel? data;

  UserDetailResponse({ this.data});

  UserDetailResponse.fromJson(Map<String, dynamic> json) {
    support = SupportModel.fromJson(json['support']);
    data = UserModel.fromJson(json['data']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['support'] = this.support;

    return data;
  }
}




class SupportModel {
  String? url;
  String? text;

  SupportModel(
      {
        this.url,
        this.text,
      });

  SupportModel.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this.url;
    data['text'] = this.text;
    return data;
  }
}


class UserModel {
  int? id;
  String? email;
  String? first_name;
  String? last_name;
  String? avatar;

  UserModel(
      {
        this.id,
        this.email,
        this.first_name,
        this.last_name,
        this.avatar
      });

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    first_name = json['first_name'];
    last_name = json['last_name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['first_name'] = this.first_name;
    data['last_name'] = this.last_name;
    data['avatar'] = this.avatar;
    return data;
  }
}
