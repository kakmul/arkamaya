import 'package:arkamaya_project/home/my_colors.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import '../config.dart';
import '../model/user_model.dart';

class UserController extends GetxController {
  final Dio dio = Dio(
    BaseOptions(
      baseUrl: Config.baseURL + Config.prefixUrl,
      connectTimeout: 50000,
      receiveTimeout: 50000,
    ),
  );


  Future<Response> get({
    required String endpoint,
    required String contentType,
  }) async {
    var response = await dio.get(
      endpoint,
      options: Options(
        headers: {
          "Content-type": contentType,
        },
      ),
    );
    return response;
  }


  Future<Response> post(
      {required String endpoint,
        required String contentType,
        dynamic body}) async {
    print("endpoint : ${endpoint}");
    print("params : ${body}");
    var response = await dio.post(endpoint,
        options: Options(
          headers: {
            "Content-type": contentType,
          },
        ),
        data: body);
    return response;
  }

  Future<UserResponse?> getUser() async {
    try {
      Response response = await get(
          endpoint: Config.get_user,
          contentType: "application/json");
      //print(response.data);
      UserResponse userResponse = UserResponse.fromJson(response.data);
      return userResponse;
    } on DioError catch (e) {
      return null;
    }
  }


  Future<UserDetailResponse?> getUserDetail({required int id}) async {
    try {
      Response response = await get(
          endpoint: Config.get_user+"/"+id.toString(),
          contentType: "application/json");
      UserDetailResponse userResponse = UserDetailResponse.fromJson(response.data);
      return userResponse;
    } on DioError catch (e) {
      return null;
    }
  }



  RxBool isLoading = false.obs;
  RxInt pageSelected = 0.obs;
  Future<UserResponse?> addUser({String? params}) async {
    isLoading.value = true;
    try {
      Response response = await post(
          endpoint: Config.get_user,
          body: params,
          contentType: "application/json");
      print(response.data);
      Get.snackbar(
        "Users Created",
        response.data.toString(),
        colorText: Colors.black,
        backgroundColor: MyColors.primaryBackground,
        icon: const Icon(Icons.check_circle,color: Colors.black,),
      );
      isLoading.value = false;

      UserResponse userResponse = UserResponse.fromJson(response.data);
      return userResponse;
    } on DioError catch (e) {
      Get.snackbar(
        "Users Create Failed",
        e.message,
        colorText: Colors.black,
        backgroundColor: MyColors.primaryBackground,
        icon: const Icon(Icons.check_circle,color: Colors.black,),
      );
      isLoading.value = false;
      return null;
    }
  }


}